"""
/***************************************************************************
 OMX and Numpy matrix vizualiser for QGIS
 
    Name:        QGIS plgin iniitalizer
                              -------------------
        begin                : 2014-03-19
        copyright            : Pedro Camargo
        Original Author: Pedro Camargo pedro@xl-optim.com
        Contributors: 
        Licence: See LICENSE.TXT
 ***************************************************************************/
"""

from qgis.core import *
import qgis
from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import *
from PyQt4.QtGui import *

import sys, os
import numpy as np
Qt = QtCore.Qt


#For the GIS tools portion
from ui_omx_visualizer import *

import operator
import time

#This class was adapted from https://www.mail-archive.com/pyqt@riverbankcomputing.com/msg17575.html
#Provided by David Douard

#adaptations for headers come from: http://stackoverflow.com/questions/14135543/how-to-set-the-qtableview-header-name-in-pyqt4

class NumpyModel(QtCore.QAbstractTableModel):
    def __init__(self, narray, headerdata,row_headersdata, parent=None):
        QtCore.QAbstractTableModel.__init__(self, parent)
        self._array = narray
        self.headerdata = headerdata
        self.row_headersdata=row_headersdata

    def rowCount(self, parent=None):
        return self._array.shape[0]

    def columnCount(self, parent=None):
        return self._array.shape[1]

    def data(self, index, role=Qt.DisplayRole):
        if index.isValid():
            if role == Qt.DisplayRole:
                row = index.row()
                col = index.column()
                return "%.5f"%self._array[row, col]
                
    def headerData(self, col, orientation, role=Qt.DisplayRole):
        if role == Qt.DisplayRole and orientation == Qt.Horizontal:
            return self.headerdata[col]
        if role == Qt.DisplayRole and orientation != Qt.Horizontal:
            return self.row_headersdata[col]
        
        return QAbstractTableModel.headerData(self, col, orientation, role)
                

#This class was adapted from http://stackoverflow.com/questions/7697194/how-can-i-set-labels-of-qheaderview-in-pyqt
#Provided by @pedrotech       
                
class Header(QHeaderView):
    def __init__(self, parent=None):
        super(Header, self).__init__(Qt.Horizontal, parent)

        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.ctxMenu)
        self.hello = QAction("Hello", self)
        self.hello.triggered.connect(self.printHello)
        self.currentSection = None

    def printHello(self):
        data = self.model().headerData(self.currentSection, Qt.Horizontal)
        print data.toString()

    def ctxMenu(self, point):
        menu = QMenu(self)
        self.currentSection = self.logicalIndexAt(point)
        menu.addAction(self.hello)
        menu.exec_(self.mapToGlobal(point))

#####################################################################################################
###############################         OMX MATRIX VIEWER          ##################################


class open_omx_matrix_class(QtGui.QDialog,Ui_omx_view_matrix):
    def __init__(self, iface):
        QtGui.QDialog.__init__(self)
        self.iface = iface
        self.setupUi(self)

        self.rows=0
        self.columns=0
        self.matrix=None
        
        self.marg_no_marg.toggled.connect(self.Works_radio_buttons)
        self.marg_sum_marg.toggled.connect(self.Works_radio_buttons)
        self.marg_max_marg.toggled.connect(self.Works_radio_buttons)
        self.marg_min_marg.toggled.connect(self.Works_radio_buttons)
        
        QObject.connect(self.matrix_cores_selector, SIGNAL("currentIndexChanged(QString)"), self.changes_cores)
        self.but_load_new_matrix.clicked.connect(self.browse_outfile)
        self.but_close.clicked.connect(self.closewidget)
        
    
    def browse_outfile(self):
        newname = QFileDialog.getOpenFileName(None, 'Result file',self.curr_matrix_path.text() , "NumPY array(*.npy);;OMX matrix(*.omx)")
        if newname == None:
            self.curr_matrix_path.setText('')
        else:
            self.curr_matrix_path.setText(newname)
            if newname[-3:].upper()=="NPY":
                self.matrix_cores_selector.clear()
                self.matrix=np.load(newname)
                self.rows=self.matrix.shape[0]
                self.columns=self.matrix.shape[1]
                self.Works_radio_buttons()
                
    def Works_radio_buttons(self):
        if self.matrix!=None:
            self.marginals=None
            if self.marg_sum_marg.isChecked(): self.marginals="SUM"
            if self.marg_max_marg.isChecked(): self.marginals="MAX"
            if self.marg_min_marg.isChecked(): self.marginals="MIN"
            row_headers=[]
            col_headers=[]
            for i in range(self.rows):
                row_headers.append(str(i))
            for j in range(self.columns):
                col_headers.append(str(j))
            if self.marginals==None:
                self.r=self.rows
                self.c=self.columns
                a=self.matrix
            else:
                row_headers.append(self.marginals)
                col_headers.append(self.marginals)
                self.r=self.rows+1
                self.c=self.columns+1
                a=np.zeros((self.matrix.shape[0]+1,self.matrix.shape[1]+1), self.matrix.dtype)
                a[0:self.r-1,0:self.c-1]=self.matrix[:,:]
                if self.marginals=="SUM":
                    a[:,self.r-1]=np.sum(a, axis=0)
                    a[self.c-1,:]=np.sum(a, axis=1)
                    a[self.c-1,self.r-1]=np.sum(self.matrix)
                elif self.marginals=="MAX":
                    a[:,self.r-1]=np.amax(a, axis=0)
                    a[self.c-1,:]=np.amax(a, axis=1)
                    a[self.c-1,self.r-1]=np.amax(self.matrix)
                elif self.marginals=="MIN":
                    a[:,self.r-1]=np.amin(a, axis=0)
                    a[self.c-1,:]=np.amin(a, axis=1)
                    a[self.c-1,self.r-1]=np.amin(self.matrix)
            
            #print row_headers
            #print col_headers
            m = NumpyModel(a, col_headers, row_headers)
            self.matrix_viewer.setModel(m)
            
            
    def closewidget(self):
        self.close()
    

    def changes_cores(self):
        core=self.matrix_cores_selector.currentText()
        
    def load_a_core_to_viewer(self):
        skims[skim_name]=(ab_field, ba_field)
        self.skims=skims
                
                
        