# -*- coding: utf-8 -*-
"""
/***************************************************************************
 OMX vizualiser for QGIS
 
    Name:        QGIS plgin iniitalizer
                              -------------------
        begin                : 2014-03-19
        copyright            : Pedro Camargo
        Original Author: Pedro Camargo pedro@xl-optim.com
        Contributors: 
        Licence: See LICENSE.TXT
 ***************************************************************************/


"""
# Import the PyQt and QGIS libraries
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *

# Import the code for the dialog
from OMX_dialogs import *
import sys
sys.dont_write_bytecode = True
import os.path
import numpy as np

class OMX_menu:

    def __init__(self, iface):
        self.iface = iface
        self.OMX_menu = None
        
    def TOOLS_add_submenu(self, submenu):
        if self.OMX_menu != None:
            self.OMX_menu.addMenu(submenu)
        else:
            self.iface.addPluginToMenu("&OMX", submenu.menuAction())
			

    def initGui(self):

        # CREATING MASTER MENU HEAD
	self.OMX_menu = QMenu(QCoreApplication.translate("OMX", "OMX"))
	self.iface.mainWindow().menuBar().insertMenu(self.iface.firstRightStandardMenu().menuAction(), self.OMX_menu)
	



#########################################################################
##################        GIS TOOLS SUB-MENU    #########################
		
        #Node to area aggregation
	icon = QIcon(os.path.dirname(__file__) + "/icons/icon_node_to_area.png")
        self.open_omx_matrix_action = QAction(icon,u"View OMX and NumPY Matrices", self.iface.mainWindow())
        QObject.connect(self.open_omx_matrix_action, SIGNAL("triggered()"), self.open_omx_matrix)
        self.OMX_menu.addAction(self.open_omx_matrix_action)

#########################################################################


        
    def unload(self):
        if self.OMX_menu != None:
            self.iface.mainWindow().menuBar().removeAction(self.OMX_menu.menuAction())
        else:
            self.iface.removePluginMenu("&OMX", self.gis_OMX_menu.menuAction())
            

    def open_omx_matrix(self):  
        dlg2 = open_omx_matrix_class(self.iface)
        dlg2.exec_()
