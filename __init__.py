"""
/***************************************************************************
 OMX vizualiser for QGIS
 
    Name:        QGIS plgin iniitalizer
                              -------------------
        begin                : 2014-03-19
        copyright            : Pedro Camargo
        Original Author: Pedro Camargo pedro@xl-optim.com
        Contributors: 
        Licence: See LICENSE.TXT
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""
import sys
sys.dont_write_bytecode = True

def classFactory(iface):
    from OMX_menu import OMX_menu
    return OMX_menu(iface)
