# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_omx_visualizer.ui'
#
# Created: Mon Mar 23 10:59:23 2015
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_omx_view_matrix(object):
    def setupUi(self, omx_view_matrix):
        omx_view_matrix.setObjectName(_fromUtf8("omx_view_matrix"))
        omx_view_matrix.resize(817, 598)
        self.centralwidget = QtGui.QWidget(omx_view_matrix)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.matrix_cores_selector = QtGui.QComboBox(self.centralwidget)
        self.matrix_cores_selector.setGeometry(QtCore.QRect(10, 30, 321, 22))
        self.matrix_cores_selector.setObjectName(_fromUtf8("matrix_cores_selector"))
        self.groupBox = QtGui.QGroupBox(self.centralwidget)
        self.groupBox.setGeometry(QtCore.QRect(350, 10, 451, 51))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.groupBox.setFont(font)
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.layoutWidget = QtGui.QWidget(self.groupBox)
        self.layoutWidget.setGeometry(QtCore.QRect(20, 20, 210, 22))
        self.layoutWidget.setObjectName(_fromUtf8("layoutWidget"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.layoutWidget)
        self.horizontalLayout.setMargin(0)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.marg_no_marg = QtGui.QRadioButton(self.layoutWidget)
        self.marg_no_marg.setChecked(True)
        self.marg_no_marg.setObjectName(_fromUtf8("marg_no_marg"))
        self.horizontalLayout.addWidget(self.marg_no_marg)
        self.marg_sum_marg = QtGui.QRadioButton(self.layoutWidget)
        self.marg_sum_marg.setObjectName(_fromUtf8("marg_sum_marg"))
        self.horizontalLayout.addWidget(self.marg_sum_marg)
        self.marg_max_marg = QtGui.QRadioButton(self.layoutWidget)
        self.marg_max_marg.setObjectName(_fromUtf8("marg_max_marg"))
        self.horizontalLayout.addWidget(self.marg_max_marg)
        self.marg_min_marg = QtGui.QRadioButton(self.layoutWidget)
        self.marg_min_marg.setObjectName(_fromUtf8("marg_min_marg"))
        self.horizontalLayout.addWidget(self.marg_min_marg)
        self.label = QtGui.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(10, 10, 71, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label.setFont(font)
        self.label.setObjectName(_fromUtf8("label"))
        self.curr_matrix_path = QtGui.QLabel(self.centralwidget)
        self.curr_matrix_path.setGeometry(QtCore.QRect(20, 500, 771, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.curr_matrix_path.setFont(font)
        self.curr_matrix_path.setObjectName(_fromUtf8("curr_matrix_path"))
        self.but_load_new_matrix = QtGui.QPushButton(self.centralwidget)
        self.but_load_new_matrix.setGeometry(QtCore.QRect(520, 530, 131, 23))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.but_load_new_matrix.setFont(font)
        self.but_load_new_matrix.setObjectName(_fromUtf8("but_load_new_matrix"))
        self.but_close = QtGui.QPushButton(self.centralwidget)
        self.but_close.setGeometry(QtCore.QRect(660, 530, 131, 23))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.but_close.setFont(font)
        self.but_close.setObjectName(_fromUtf8("but_close"))
        self.matrix_viewer = QtGui.QTableView(self.centralwidget)
        self.matrix_viewer.setGeometry(QtCore.QRect(10, 80, 791, 411))
        self.matrix_viewer.setObjectName(_fromUtf8("matrix_viewer"))
        self.statusbar = QtGui.QStatusBar(omx_view_matrix)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        #omx_view_matrix.setStatusBar(self.statusbar)

        self.retranslateUi(omx_view_matrix)
        QtCore.QMetaObject.connectSlotsByName(omx_view_matrix)

    def retranslateUi(self, omx_view_matrix):
        omx_view_matrix.setWindowTitle(_translate("omx_view_matrix", "MainWindow", None))
        self.groupBox.setTitle(_translate("omx_view_matrix", "Marginals", None))
        self.marg_no_marg.setText(_translate("omx_view_matrix", "None", None))
        self.marg_sum_marg.setText(_translate("omx_view_matrix", "Sum", None))
        self.marg_max_marg.setText(_translate("omx_view_matrix", "Max", None))
        self.marg_min_marg.setText(_translate("omx_view_matrix", "Min", None))
        self.label.setText(_translate("omx_view_matrix", "Matrix cores", None))
        self.curr_matrix_path.setText(_translate("omx_view_matrix", "No Matrix Loaded", None))
        self.but_load_new_matrix.setText(_translate("omx_view_matrix", "Load new matrix", None))
        self.but_close.setText(_translate("omx_view_matrix", "Close", None))

